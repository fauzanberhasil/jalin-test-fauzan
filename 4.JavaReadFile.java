import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class JavaReaderfile {
    public static void main(String[] args) {
        String inputFilePath = "/input"; // Input file path
        String client = args[0]; // Client choice (BNI or MDR)

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        // Schedule the program to run once a day
        executorService.scheduleAtFixedRate(() -> {
            try {
                // Read the input file
                String input = readFile(inputFilePath);

                // Generate an output message based on the selected client
                String outputMessage = generateOutputMessage(client, input);

                // Send the output message to the institutions (You can implement this part)
                

                System.out.println("Output Message: " + outputMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, 0, 1, TimeUnit.DAYS); // Run once a day

        // Keep the program running
        while (true) {
            // The scheduler will keep running the task daily
        }
    }

    private static String readFile(String filePath) throws IOException {
        StringBuilder content = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)) {
            String line;
            while ((line = br.readLine()) != null) {
                content.append(line).append("\n");
                String[] parts = line.split(";");
                String[] rows;
                if (parts.length >= 3) {
                    String a = parts[0].trim();
                    String b = parts[1].trim();
                    String c = parts[2].trim();

                    if (parts.length >= 5) {
                        String d = parts[4].trim();
                        if ("offline".equalsIgnoreCase(d)) {
                            // Perform actions when d is "offline"
                            System.out.println("a: " + a);
                            System.out.println("b: " + b);
                            System.out.println("c: " + c);
                            System.out.println("d: " + d);
                            rows = a+" : Envi "+b+" Port "+c+" terpantau "+d;
                        }
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    private static String generateOutputMessage(String client, String input) {
        // Implement your logic to generate the output message based on the selected client
        // Example logic:
        if ("BNI".equals(client)) {
            return "Output message for BNI: " + input;
        } else if ("MDR".equals(client)) {
            return "Output message for MDR: " + input;
        } else {
            return "Invalid client choice";
        }
    }
}