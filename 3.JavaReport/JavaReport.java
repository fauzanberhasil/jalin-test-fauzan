import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DailyScheduledTask {

    public static void main(String[] args) {
        // Create a scheduler to run the task once a day
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        
        // Replace these with your database credentials
        String dbUrl = "jdbc:mysql://localhost";
        String dbUser = "admin";
        String dbPassword = "123";

        // Input parameter
        String codeClient = "your_code_client"; // Replace with the actual input
        Date previousDate = "tanggal parameter";

        // Schedule the task
        scheduler.scheduleAtFixedRate(() -> {
            try {
                // Connect to the database
                Connection conn = DriverManager.getConnection(dbUrl, dbUser, dbPassword);

                
                
                // Prepare a SQL query
                String sql = "SELECT code_client, tanggal_transaksi, waktu_transaksi, status, tagihan, admin, nominal " +
                             "FROM your_table_name " +
                             "WHERE code_client in ?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, codeClient);
                stmt.setDate(2, new java.sql.Date(previousDate.getTime()));

                // Execute the query
                ResultSet result = statement.executeQuery();

                // Process the result
                while (result.next()) {
                    String clientCode = result.getString("code_client");
                    Date tanggal = rs.getDate("tanggal");
                    Date tanggal_transaksi = rs.getDate("tanggal_transaksi");
                    String transactionTime = result.getString("waktu_transaksi");
                    String status = result.getString("status");
                    double tagihan = result.getDouble("tagihan");
                    double adminFee = result.getDouble("admin");
                    double nominal = result.getDouble("nominal");

                    // Perform your desired operations with the retrieved data
                    // For example, print the values
                    System.out.println("Client Code: " + clientCode);
                    System.out.println("Transaction Date: " + transactionDate);
                    System.out.println("Transaction Time: " + transactionTime);
                    System.out.println("Status: " + status);
                    System.out.println("Tagihan: " + tagihan);
                    System.out.println("Admin Fee: " + adminFee);
                    System.out.println("Nominal: " + nominal);
                }

                // Close resources
                result.close();
                statement.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }, 0, 1, TimeUnit.DAYS); // Run the task once a day

        // Add any additional code or logic you need here

        // Shut down the scheduler gracefully when done
        //scheduler.shutdown();
    }
}
