CREATE TABLE pembayaran (
    id INT PRIMARY KEY,
    code_client VARCHAR(20), 
    tanggal DATE,
    tanggal_transaksi DATE,
    waktu_transaction TIME,
    status VARCHAR(50), 
    tagihan DECIMAL(10, 2), 
    admin DECIMAL(10, 2), 
    nominal DECIMAL(10, 2)
);