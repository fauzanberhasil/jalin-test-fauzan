-- ### Find the connection for each table:
-- a.cardnumber = b.cardnumber = c.cardnumber
-- a.iss = b.iss = c.iss
-- a.acq = b.acq = c.acq
-- a.dest = b.dest = c.dest
-- a.status AS c.status_a
-- b.status AS c.status_iss when b.source = a.iss
-- b.status AS c.status_acq when b.source = a.acq

-- Menggunakan cursor untuk mengambil data dari tabel A dan B
DECLARE
  CURSOR data_cursor IS
    SELECT
    a.id,
    a.cardnumber,
    a.iss,
    a.acq,
    a.dest,
    a.status AS status_a,
    CASE
        WHEN b.source = a.iss THEN b.status
        ELSE null
    END AS status_iss,
    CASE
        WHEN b.source = a.acq THEN b.status
        ELSE null
    END AS status_acq,
    0 AS status_dest
    FROM a
    INNER JOIN b ON a.cardnumber = b.cardnumber
            AND a.iss = b.iss
            AND a.acq = b.acq
            AND a.dest = b.dest;

BEGIN
  -- Iterasi melalui data yang diambil dari cursor
  FOR data_row IN data_cursor LOOP
    -- Memasukkan data ke tabel C
    INSERT INTO c (id, cardnumber, iss, acq, dest, status_a, status_iss, status_acq, status_dest)
    VALUES (data_row.id, data_row.cardnumber, data_row.iss, data_row.acq, data_row.dest, data_row.status_a, data_row.status_iss, data_row.status_acq, data_row.status_dest);
  END LOOP;
END;
/

